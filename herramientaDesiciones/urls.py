from django.conf.urls import url
from django.contrib import admin
from .views import *

urlpatterns = [
    url(r'^$', HomePageViewHerramienta, name='index-herramienta'),
    url(r'^consulta/$', consulta_herramienta, name='index_consulta'),

    #ajax
    #url(r'^ajax/deptos/$', get_deptos, name='get-deptos'),
    #url(r'^ajax/municipios/$', get_munis, name='get-munis'),
    url(r'^api/productor/$', get_productor, name='productor-search'),
    url(r'^mapaherramienta/$', obtener_lista_mapa_cacao, name='obtener-lista-mapa-cacao'),
    url(r'^(?P<vista>\w+)/$', get_view, name='get-view'),
]
