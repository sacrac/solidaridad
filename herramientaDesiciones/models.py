# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from lugar.models import Pais, Departamento, Municipio, Comunidad
from smart_selects.db_fields import ChainedForeignKey
from multiselectfield import MultiSelectField

# Create your models here.

CHOICE_SEXO = (
                (1, 'Mujer'),
                (2, 'Hombre'),
              )

CHOICE_MANEJA_FINCA = (
                (1, 'Hombre'),
                (2, 'Mujer'),
                (3, 'Ambos'),
                (4, 'Entre varios')
              )

class Encuestadores(models.Model):
    nombre = models.CharField(max_length=250)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Encuestador'
        verbose_name_plural = 'Encuestadores'
        unique_together = ('nombre',)


class Entrevistados(models.Model):
    nombre = models.CharField('Nombre dueño/a de la finca', max_length=250)
    nombre_finca = models.CharField('Nombre de la finca', max_length=250)
    sexo = models.IntegerField(choices=CHOICE_SEXO)
    pais = models.ForeignKey(Pais)
    departamento = ChainedForeignKey(
        Departamento,
        chained_field="pais",
        chained_model_field="pais",
        show_all=False,
        auto_choose=True
    )
    municipio = ChainedForeignKey(
        Municipio,
        chained_field="departamento",
        chained_model_field="departamento",
        show_all=False,
        auto_choose=True
    )
    comunidad = ChainedForeignKey(
        Comunidad,
        chained_field="municipio",
        chained_model_field="municipio",
        show_all=False,
        auto_choose=True,
        null=True,
        blank=True,
    )
    latitud = models.FloatField(null=True, blank=True)
    longitud = models.FloatField(null= True, blank=True)
    altitud = models.FloatField('Altitud de la finca', null=True,blank=True)
    maneja = models.IntegerField(choices=CHOICE_MANEJA_FINCA, verbose_name='Quién maneja la finca')

    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = '1.1 Info. general de la finca y productor'
        verbose_name_plural = '1.1 Info. general de la fincas y productores'
        unique_together = ('nombre',)

class Organizacion(models.Model):
    nombre = models.CharField(max_length=250)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Organización'
        verbose_name_plural = 'Organizaciones'

CHOICE_DESDE_CUANDO = (
                (1, 'Menos de 5 años'),
                (2, 'Más de 5 años'),
                (3, 'Ninguno')
              )

CHOICE_SOCIOS = (
                ('A', 'Cooperativa'),
                ('B', 'Asociación'),
                ('C', 'Empresa'),
                ('D', 'Grupo'),
                ('E', 'Ninguno')
              )

class Encuesta(models.Model):
    fecha = models.DateField()
    encuestador = models.ForeignKey(Encuestadores)
    entrevistado = models.ForeignKey(Entrevistados)
    socio = MultiSelectField('Es socio/a de una organizacion gremial', choices=CHOICE_SOCIOS, null=True, blank=True)
    organizacion = models.ForeignKey(Organizacion, verbose_name='Nombre de la organización')
    desde_cuando = models.IntegerField(choices=CHOICE_DESDE_CUANDO)

    year = models.IntegerField(editable=False, verbose_name='Año')
    user = models.ForeignKey(User)

    def save(self):
        self.year = self.fecha.year
        super(Encuesta, self).save()

    def __unicode__(self):
        return u'%s' % (self.entrevistado.nombre)

    class Meta:
        verbose_name_plural = 'ENCUESTAS'


CHOICE_EDUCACION = (
                (1, 'No Sabe Leer/Escribir'),
                (2, 'Primaria Incompleta'),
                (3, 'Primaria completa'),
                (4, 'Secundaria Incompleta'),
                (5, 'Bachiller'),
                (6, 'Universidad')
              )

class ComposicionFamilia(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    #Número de personas que depende de la finca (Familia)
    varones_adultos = models.IntegerField('Número de adultos varones')
    mujeres_adultas = models.IntegerField('Número de adultas mujeres')
    varones_jovenes = models.IntegerField('Número de Jóvenes varones (11-16 años)')
    mujeres_jovenes = models.IntegerField('Número de Jóvenes mujeres (11-16 años)')
    ninos = models.IntegerField('Números niños (1-10 años)')
    ninas = models.IntegerField('Números niñas (1-10 años)')
    #Relación finca-vivienda (relacionado al dueño(s) de la finca)
    viven_finca = models.IntegerField('Viven en la finca')
    viven_pueblo = models.IntegerField('Viven en pueblo cerca de finca')
    viven_ciudad = models.IntegerField('Vive en ciudad lejos de la finca')
    viven_finca_casa = models.IntegerField('Pasa tiempo en la finca y tiempo en casa')
    #Número de trabajadores que laboran en la finca
    permanentes_hombre = models.IntegerField('Trabajadores permanentes hombres')
    permanentes_mujeres = models.IntegerField('Trabajadores permanentes mujeres')
    temporal_hombre = models.IntegerField('Trabajadores temporales hombres')
    temporal_mujeres = models.IntegerField('Trabajadores temporales mujeres')
    tecnicos_hombre = models.IntegerField('Trabajadores tecnicos hombres')
    tecnicos_mujeres = models.IntegerField('Trabajadores tecnicos mujeres')
    #nivel de educacion del duano de la finca
    nivel_educacion = models.IntegerField(choices=CHOICE_EDUCACION,
                                    verbose_name='Nivel de educación de dueño de la finca?')

    class Meta:
        verbose_name_plural = '1.3 Composicion del grupo familia'


CHOICE_ELECTRICIDAD = (
                ('A', 'No hay'),
                ('B', 'La red'),
                ('C', 'Planta eléctrica'),
                ('D', 'Panel Solar'),
                ('E', 'Molino de viento'),
                ('F', 'Candil')
              )

CHOICE_COMBUSTIBLE = (
                ('A', 'Gas'),
                ('B', 'Leña de la finca'),
                ('C', 'Leña comprada'),
                ('D', 'Carbón'),
                ('E', 'Candela'),
                ('F', 'Foco')
              )

CHOICE_FUENTE_AGUA = (
                ('A', 'Río'),
                ('B', 'Ojo de agua'),
                ('C', 'Quebrada'),
                ('D', 'Pozo comunitario'),
                ('E', 'Pozo propio'),
                ('F', 'Agua entubada')
              )

class ServiciosFinca(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    electricidad = MultiSelectField('Disponibilidad de energía',
                                                choices=CHOICE_ELECTRICIDAD, null=True, blank=True)
    combustible = MultiSelectField('Combustible en la cocina',
                                                choices=CHOICE_COMBUSTIBLE, null=True, blank=True)
    agua_trabajo = MultiSelectField('Fuente de agua para trabajo de la finca',
                                                choices=CHOICE_FUENTE_AGUA, null=True, blank=True)
    agua_consumo = MultiSelectField('Fuente de agua para consumo humano en la finca',
                                                choices=CHOICE_FUENTE_AGUA, null=True, blank=True)


    class Meta:
        verbose_name_plural = '1.4 Servicios  básicos en la finca'

CHOICE_TENENCIA = (
                (1, 'Propia con escritura pública'),
                (2, 'Propias con Promesa de venta'),
                (3, 'Propias con Escritura posesoria'),
                (4, 'Propia por herencia'),
                (5, 'Propias con título de reforma agraria'),
                (6, 'Propia Sin documento'),
                (7, 'Parcela en tierra comunitaria'),
                (8, 'Arrendada')
              )

CHOICE_DOCUMENTO_LEGAL = (
                (1, 'Hombre'),
                (2, 'Mujer'),
                (3, 'Mancomunado'),
                (4, 'Parientes'),
                (5, 'Colectivo'),
                (6, 'No hay'),
              )

class Tenencia(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    tenencia_parcela = models.IntegerField(choices=CHOICE_TENENCIA)
    documento_legal = models.IntegerField(choices=CHOICE_DOCUMENTO_LEGAL)

    class Meta:
        verbose_name_plural = '1.5 Tenencia de la tierra'

CHOICE_ALIMENTOS_COMPRA = (
                (1, 'Todo'),
                (2, 'Más de la Mitad'),
                (3, 'Menos de la Mitad'),
                (4, 'Nada'),
              )


CHOICE_SI_NO = (
                (1, 'Si'),
                (2, 'No'),
              )

CHOICE_MESES = (
                ('A', 'Enero'),
                ('B', 'Febrero'),
                ('C', 'Marzo'),
                ('D', 'Abril'),
                ('E', 'Mayo'),
                ('F', 'Junio'),
                ('G', 'Julio'),
                ('H', 'Agosto'),
                ('I', 'Septiembre'),
                ('J', 'Octubre'),
                ('K', 'Noviembre'),
                ('L', 'Diciembre'),
              )

class SeguridadAlimentario(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    alimentos_basico = models.IntegerField('¿Qué parte de alimentos básicos que consume la familia se compra?',
                                    choices=CHOICE_ALIMENTOS_COMPRA)
    cubrir_necesidades_basicas = models.IntegerField('¿Siente que en algunos años no ha podido cubrir las necesidades básicas de alimentación de la familia o la finca?',
                                                        choices=CHOICE_SI_NO)
    meses_dificiles = MultiSelectField('¿Cuáles son los meses más difíciles para la alimentación de la familia o la finca?',
                                                choices=CHOICE_MESES)

    class Meta:
        verbose_name_plural = '1.6 Seguridad alimentaria de la familia'

#2 el sistema de produccion en la finca

CHOICE_TIERRA = (
        (1, 'Área total'),
        (2, 'Bosque'),
        (3, 'Tacotal'),
        (4, 'Cultivo anual'),
        (5, 'Plantación forestal'),
        (6, 'Áreas de pastos abierto'),
        (7, 'Áreas de pastos con árboles'),
        (8, 'Cultivos perennes'),
    )

class UsoTierra(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    tierra = models.IntegerField(choices=CHOICE_TIERRA, verbose_name='Uso de la tierra')
    manzanas = models.FloatField('Áreas en Mz')

    class Meta:
        verbose_name_plural = '2.1 Uso de la tierra en la finca'


class Rubros(models.Model):
    nombre = models.CharField(max_length=250)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Rubros'
        unique_together = ('nombre',)

CHOICES_FIN_PRODUCCION = (
                    (1, 'Autoconsumo'),
                    (2, 'Venta'),
                    (3, 'Ambos'),
    )

class RubrosFinca(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    rubros = models.ForeignKey(Rubros)
    producen_finca = models.IntegerField(choices=CHOICE_SI_NO)
    fin_produccion = models.IntegerField(choices=CHOICES_FIN_PRODUCCION)

    class Meta:
        verbose_name_plural = '2.3 Rubros en la finca'

# 3 sistema produccion cacao en la finca

CHOICES_ESTADO_ACTUAL = (
                    (1, 'Área total de cacao'),
                    (2, 'Área de cacao en desarrollo'),
                    (3, 'Área de cacao en producción'),
                    (4, 'Producción total de cacao en qq baba'),
    )

class EstadoActual(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opciones = models.IntegerField(choices=CHOICES_ESTADO_ACTUAL)

    class Meta:
        verbose_name_plural = '3.1a Área de cacao'

CHOICES_ANIOS = (
                (1, '2014'),
                (2, '2015'),
                (3, '2016'),
                (4, '2017'),
                (5, '2018'),
                (6, '2019'),
                (7, '2020'),
                (8, '2021'),
        )

class YearEstado(models.Model):
    estado = models.ForeignKey(EstadoActual)
    anio = models.IntegerField(choices=CHOICES_ANIOS)
    valor = models.FloatField()

    class Meta:
        verbose_name_plural = 'años'

CHOICE_LIMITANTE_PRODUCCION = (
                ('A', 'Falta de crédito'),
                ('B', 'Comercialización'),
                ('C', 'Falta de mano de obra'),
                ('D', 'Falta de capacitación / asistencia técnica'),
              )

CHOICES_LIMITANTE_BIOFISICO = (
                ('A', 'Fertilidad de suelo'),
                ('B', 'Variedades no productivas'),
                ('C', 'Monilia'),
                ('D', 'Ardillas'),
                ('E', 'Sombra/Poda'),
                ('F', 'Exceso de lluvia'),
                ('G', 'Falta de lluvia'),
    )


class Factores(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    factor_socioeconomico = MultiSelectField('Factores socioeconómicos',
                                                choices=CHOICE_LIMITANTE_PRODUCCION)
    factor_biofisico = MultiSelectField('Factores limitantes biofísicos',
                                                choices=CHOICES_LIMITANTE_BIOFISICO)

    class Meta:
        verbose_name_plural = '3.1 b Factores Limitantes la Producción de Cacao'

CHOICES_VARIEDADES = (
                ('A', 'Clone'),
                ('B', 'Híbrido'),
                ('C', 'Cacao criollo'),
                ('D', 'Planta élite'),
                ('E', 'Mezcla'),
    )

# class Variedad(models.Model):
#     encuesta = models.ForeignKey(Encuesta)
#     nombre_parcela = models.CharField(max_length=250)
#     area = models.FloatField('Área en Mz')
#     edad = models.FloatField('Edad en años')
#     variedad = MultiSelectField('Variedades',
#                                                 choices=CHOICES_VARIEDADES)
#     produccion = models.FloatField('Producción en qq baba de último año')
#     nivel = models.FloatField('Nivel de Monilia año pasado basado en % mazorcas afectadas')

#     class Meta:
#         verbose_name_plural = '3.2 Variedad, edad, Monilia y producción'

CHOICES_CONSIGUEN_SEMILLA = (
                ('A', 'La misma finca'),
                ('B', 'Fincas vecinas'),
                ('C', 'Vivero Cooperativa'),
                ('D', 'Vivero Empresa'),
    )

class LaProduccion(models.Model):
     encuesta = models.ForeignKey(Encuesta)
     tiene_vivero = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Actualmente tiene vivero de cacao en la finca?')
     cuantas = models.FloatField('¿Cuántas plantas hay en el vivero?')
     variedad = MultiSelectField('¿Qué variedades hay en el vivero?', choices=CHOICES_VARIEDADES)
     consiguen = MultiSelectField('¿Actualmente donde consiguen las semillas?', choices=CHOICES_CONSIGUEN_SEMILLA)

     class Meta:
        verbose_name_plural = '3.3 La producción de vivero y su disponibilidad a los productores'


CHOICES_MANEJO_1 = (
                (1, 'Calidad de manejo'),
        )

CHOICES_MANEJO_2 = (
                (1, 'Nivel de uso de manejo'),
        )

CHOICES_MANEJO_3 = (
                (1, 'Mes que realizaron los labores'),
        )

CHOICES_MANEJO_4 = (
                (1, 'Quiénes realizan las labores?'),
        )

CHOICES_CALIDAD = (
                (1, 'Bueno'),
                (2, 'Regular'),
                (3, 'Mal'),
                (4, 'No hace'),
        )

class CalidadManejo(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_1)
    poda = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Poda de cacao')
    sombra = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de sombra' )
    maleza = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de malezas')
    fertilidad = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '3.4.1- Calidad de manejo'

CHOICES_USO_MANEJO = (
                (1, 'En todos los plantios'),
                (2, 'En varios platios'),
                (3, 'Solamente en algunos plantíos'),
                (4, 'En ninguno plantío')
        )

class NivelManejo(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_2)
    poda = models.IntegerField(choices=CHOICES_USO_MANEJO, verbose_name='Poda de cacao')
    sombra = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de sombra' )
    maleza = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de malezas')
    fertilidad = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '3.4.2- Nivel de uso de manejo'


class MesLabores(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_3)
    poda = MultiSelectField(choices=CHOICE_MESES,verbose_name='Poda de cacao')
    sombra = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de sombra' )
    maleza = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de malezas')
    fertilidad = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '3.4.3- Mes que realizaron los labores'

CHOICES_REALIZAN_LABOR = (
                ('A', 'Hombre'),
                ('B', 'Mujeres'),
                ('C', 'Niños o Niñas'),
                ('D', 'Trabajadores'),
    )

class RealizanLabores(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opcion = models.IntegerField(choices=CHOICES_MANEJO_4)
    poda = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Poda de cacao')
    sombra = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de sombra' )
    maleza = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de malezas')
    fertilidad = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = MultiSelectField(choices=CHOICES_REALIZAN_LABOR,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '3.4.4- Quiénes realizan las labores?'

CHOICES_OPCIONES_AGRO = (
                (1, 'Biofertilzantes'),
                (2, 'Compost'),
                (3, 'Roca Minerales'),
                (4, 'Insecticida natural'),
                (5, 'Fungicida natural'),
                (6, 'Cerca viva'),
                (7, 'Cortina rompe viento'),
                (8, 'Abonos verdes'),
                (9, 'Siembra en Curva a nivel'),
                (10, 'Acequia'),
                (11, 'Barrera viva'),
                (12, 'Barrera muerta'),
                (13, 'Cosecha de agua'),
                (14, 'Incorporación de rastrojo'),
                (15, 'Manejo selectivo de malas hierbas'),
                (16, 'Siembra de coberturas'),
                (17, 'Siembra de plantas injertadas'),
                (18, 'Diversificación de los cacaotales'),
                (19, 'Eliminación de mazorcas enfermas'),
        )

CHOICES_ESCALAS = (
                (1, 'No utiliza'),
                (2, 'En pequeño escala'),
                (3, 'En mayor escala'),
                (4, 'En toda la finca'),
            )

class OpcionesAgroecologicas(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opciones = models.IntegerField(choices=CHOICES_OPCIONES_AGRO)
    nivel = models.IntegerField(choices=CHOICES_ESCALAS)

    class Meta:
        verbose_name_plural = '3.6 Uso de opciones agroecológicas en los cacaotales'

CHOICES_CORTES = (
                ('A', 'Con machete'),
                ('B', 'Con tijera'),
                ('C', 'Con media luna'),
                ('D', 'Con dejarretadora'),
            )

CHOICES_SEPARAN_MAZORCA = (
                (1, 'Si'),
                (2, 'No'),
                (3, 'Algunas veces'),
            )

CHOICES_QUIEBRAN_MAZORCA = (
                (1, 'Con Cutacha'),
                (2, 'Con mazo'),
                (3, 'Con piedra'),
                (4, 'Con machete'),
            )

class Cosecha(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    cortes = MultiSelectField(choices=CHOICES_CORTES,
                    verbose_name='¿Cómo realizaron los cortes?', null=True, blank=True)
    separan_mazorca = models.IntegerField(choices=CHOICES_SEPARAN_MAZORCA, verbose_name='¿Separan diferentes tipos de mazorcas?')
    quiebran_mazorca = models.IntegerField(choices=CHOICES_QUIEBRAN_MAZORCA, verbose_name='¿Cómo quiebran las mazorcas?')
    calidad  = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Conoce la calidad de su cacao?')
    determina_calidad = models.CharField('¿Quién determino la calidad de su cacao?', max_length=250)
    precio  = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Reciben un sobre precio para su cacao?')
    cuanto = models.FloatField('¿Cuánto por qq baba?', null=True, blank=True)

    class Meta:
        verbose_name_plural = '3.7 Cosecha'

CHOICES_COMERCIALIZACION = (
                (1, 'Cacao en baba'),
                (2, 'Cacao seco'),
            )

class Comercializacion(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opcion = models.IntegerField(choices=CHOICES_COMERCIALIZACION)
    cantidad_total = models.FloatField('Cantidad en qq Total')
    intermediario = models.FloatField('Intermediarios Cantidad en qq')
    intermediario_precio = models.FloatField('Intermediarios Precio pagado en C$ por qq')
    coop_cantidad = models.FloatField('Cooperativa Cantidad en qq')
    coop_precio = models.FloatField('Cooperativa Precio pagado en C$ por qq')

    class Meta:
        verbose_name_plural = '3.8 La comercialización'

CHOICES_CREDITO = (
                (1, 'Ultimo año'),
            )

CHOICES_FACILIDAD = (
                (1, 'Facil'),
                (2, 'Regular'),
                (3, 'Dificil'),
            )

class Credito(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opcion = models.IntegerField(choices=CHOICES_CREDITO)
    monto_credito_corto = models.FloatField('Monto de crédito corto plazo')
    monto_credito_mediano = models.FloatField('Monto de crédito de mediano plazo')
    monto_credito_largo = models.FloatField('Monto de crédito de largo plazo')
    facilidad = models.IntegerField(choices=CHOICES_FACILIDAD)

    class Meta:
        verbose_name_plural = '3.9.1 Crédito para producción de cacao'

CHOICES_OBTIENE_CREDITO = (
                (1, 'Empresa Ex'),
                (2, 'ONG'),
                (3, 'Micro Finanzas'),
                (4, 'Banco'),
                (5, 'Coop'),
                (6, 'Proyecto'),
            )

class ObtieneCredito(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    obtiene_credito = MultiSelectField(choices=CHOICES_OBTIENE_CREDITO,
                                    verbose_name='¿De quién obtiene el crédito?')

    class Meta:
        verbose_name_plural = '3.9.2 Obtiene crédito para producción de cacao'

CHOICES_CADA_CUANTO = (
            (1,'Mensual'),
            (2,'Cada 3 meses'),
            (3,'Cada 6 meses'),
            (4,'Anual'),
    )

CHOICES_COMO_REALIZA = (
            (1,'Observaciones'),
            (2,'Recuentos'),
    )

CHOICES_LLEVA_REGISTROS = (
            (1,'Si'),
            (2,'No'),
            (3, 'Si y Procesa y usa los datos')
    )

CHOICES_FALTA_RECURSO = (
            (1,'Insumos'),
            (2,'Pago de Mano de obra'),
            (3, 'Gastos operativos'),
            (4, 'Inversiones'),
    )

CHOICES_TIPO_CERTIFICADO = (
            ('A','CJ'),
            ('B','ORG'),
            ('C', 'RFA'),
            ('D', 'UTZ'),
            ('E', 'SPP'),
            ('F', 'Sin Certificación'),
            ('G', 'En proceso'),
    )

CHOICES_APOYA_PLANES = (
            ('A','Propio'),
            ('B','Contrate AT'),
            ('C', 'ONG'),
            ('D', 'Empresas'),
            ('E', 'Cooperativa'),
            ('F', 'Asoc'),
            ('G', 'Banco o Micro-finanza'),
    )

class MitigacionRiesgo(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    monitoreo_plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='¿Realiza monitoreo de plagas y enfermedades?')
    cada_cuanto = models.IntegerField(choices=CHOICES_CADA_CUANTO,
                                verbose_name='¿Cada cuánto realiza monitoreo de plagas y enfermedades?')
    como_realiza = models.IntegerField(choices=CHOICES_COMO_REALIZA,
                                verbose_name='¿Cómo realiza monitoreo de plagas y enfermedades?')
    lleva_registro = models.IntegerField(choices=CHOICES_LLEVA_REGISTROS,
                                verbose_name='¿Si lleva registro de monitoreo de plagas y enfermedades?')
    cuenta_recursos = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con suficiente recursos para manejo de finca?')
    falta_recurso = MultiSelectField(choices=CHOICES_FALTA_RECURSO,
                                verbose_name='¿Para qué cosas hace falta los recursos?' )
    obras_almacenamiento = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con obras para almacenamiento de agua?')
    venta_organizada = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Vende su Cacao en forma organizada?')
    contrato_venta = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con un contrato para la venta de cacao?')
    certificado = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Está certificado los cacaotales?')
    tipo_certificado = MultiSelectField(choices=CHOICES_TIPO_CERTIFICADO,
                                verbose_name='¿Qué tipo de certificación?')
    calidad_cacao = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿La calidad de su cacao en reconocida y monitoreada?')
    plan_manejo = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con plan de manejo de la finca?')
    plan_negocio = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con un plan de negocio para el cultivo de cacao?')
    plan_inversion = models.IntegerField(choices=CHOICE_SI_NO,
                                        verbose_name='¿Cuenta con un plan de inversión para el cultivo de cacao?')
    apoya_planes = MultiSelectField(choices=CHOICES_APOYA_PLANES,
                                verbose_name='¿Quién apoyo para elaborar estos planes?')

    class Meta:
        verbose_name_plural = '3.10 Mitigación de los riesgos'

CHOICES_CAPACITACION_TECNICA = (
            (1,'Variedades de cacao'),
            (2,'Manejo de semillero y vivero'),
            (3,'Establecimiento y manejo de cacao'),
            (4,'Manejo de suelo y fertilidad de suelo'),
            (5,'Manejo de cosecha de cacao'),
            (6,'Manejo de plagas y enfermedades'),
            (7,'Poda/ Manejo de sombra'),
            (8,'Enjertación'),
            (9,'Planificación de la finca'),
            (10,'Diversificación de la finca'),
    )

CHOICES_NIVEL_CONOCIMIENTO = (
            (1,'Pobre'),
            (2,'Algo'),
            (3,'Regular'),
            (4,'Bueno'),
            (5,'Excelente'),
    )
class CapacitacionTecnicas(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    opcion = models.IntegerField(choices=CHOICES_CAPACITACION_TECNICA)
    cuantas_veces = models.IntegerField('¿Cuántas veces?')
    capacito = models.CharField('¿Quién le capacitó?', max_length=250)
    nivel_consideracion = models.IntegerField(choices=CHOICES_NIVEL_CONOCIMIENTO)

    class Meta:
        verbose_name_plural = '3.11 Capacitaciones técnica recibida en el último año'

CHOICES_TEMAS_SOCIAL = (
            (1,'Formación y fortalecimiento organizacional'),
            (2,'Contabilidad básica y administración'),
            (3,'Manejo de créditos'),
            (4,'Administración de pequeños negocios'),
            (5,'Gestión empresarial'),
            (6,'Registro de datos de la finca'),
            (7,'Certificación de cacao'),

    )

class CapacitacionSocial(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    tema = models.IntegerField(choices=CHOICES_TEMAS_SOCIAL)
    cuantas_veces = models.IntegerField('¿Cuántas veces?')
    capacito = models.CharField('¿Quién le capacitó?', max_length=250)
    nivel_consideracion = models.IntegerField(choices=CHOICES_NIVEL_CONOCIMIENTO)

    class Meta:
        verbose_name_plural = '3.12 Capacitaciones en tema social recibidas en el último año'

CHOICE_TIPO_PLANTA = (
            (1,'En desarrollo'),
            (2,'Productivo'),
            (3,'Mezcla'),
    )

CHOICES_VARIEDAD_PREDOMINANTE = (
            ('A','Clone'),
            ('B','Hibrido'),
            ('C', 'Mezcla'),
           ('D', 'Criolla'),
           ('E', 'Planta élite'),
    )

CHOICE_TIPO_SOMBRA = (
            (1,'Montaña'),
            (2,'Guineo'),
            (3,'Mezclado'),
            (4,'Guaba'),
    )

CHOICE_TIPO_FERTILIZACION = (
            (1,'Orgánico'),
            (2,'Químico'),
            (3,'Quimico + Orgánico'),
            (4,'Ninguno'),
    )

CHOICE_TIPO_FUNGICIDA = (
            (1,'Quimico Prev'),
            (2,'Quimico Sist'),
            (3,'Orgánico'),
            (4,'Ninguno'),
    )

class DetallePlantios(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    nombre_plantio = models.CharField('Nombre de la parcela', max_length=150)
    area = models.FloatField('Área en Mz')
    tipo_planta = models.IntegerField(choices=CHOICE_TIPO_PLANTA)
    edad = models.FloatField('Edad en años')
    variedades = MultiSelectField(choices=CHOICES_VARIEDAD_PREDOMINANTE,
                                verbose_name='Variedades Predominante' )
    produccion = models.FloatField('Producción en qq baba de último año')
    nivel = models.FloatField('Nivel de Monilia año pasado basado en % mazorcas afectadas')
    #final de la encuesta
    tipo_sombra = models.IntegerField(choices=CHOICE_TIPO_SOMBRA)
    nivel_sombra = models.FloatField('Nivel de sombra %')
    tipo_fertilizacion = models.IntegerField(choices=CHOICE_TIPO_FERTILIZACION)
    tipo_fungicida = models.IntegerField(choices=CHOICE_TIPO_FUNGICIDA)
    rendimientos = models.FloatField(editable=False)

    def save(self, *args, **kwargs):
        self.rendimientos = float(self.produccion) / float(self.area)
        super(DetallePlantios, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Detalles de algunos plantíos de cacao (en base de visita de campo)'


class DetalleSeleccion(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    parcela_seleccionada = models.CharField('Parcela Seleccionado como parcela de aprendizaje?', max_length=250)
    porque = models.TextField()

    class Meta:
        verbose_name_plural = 'Parcela seleccionada de aprendizaje'
