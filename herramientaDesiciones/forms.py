# -*- coding: utf-8 -*-
from dal import autocomplete

from django import forms
from herramientaDesiciones.models import Encuesta, Organizacion
from lugar.models import *

class EntrevistadoForm(forms.ModelForm):
    class Meta:
        model = Encuesta
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }


def fecha_choice():
    years = []
    for en in Encuesta.objects.order_by('fecha').values_list('fecha', flat=True):
        years.append((en.year,en.year))
    return list(sorted(set(years)))


CHOICE_SEXO1 = (
    ('', '-------'),
    (1, 'Mujer'),
    (2, 'Hombre')
)

class ConsultaHerramientaForm(forms.Form):
    fecha = forms.MultipleChoiceField(choices=fecha_choice(), label="Años", required=False)
    #ciclo = forms.MultipleChoiceField(choices=ciclo_choice(), required=False)
    productor = forms.CharField(max_length=250, required=False)
    organizacion = forms.ModelChoiceField(queryset=Organizacion.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=Pais.objects.all(), required=False)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.all(), required=False)
    comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.all(), required=False)
    sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)
    #tipologia = forms.ChoiceField(choices=CHOICE_TIPOLOGIA1, required=False)
