# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-28 23:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('herramientaDesiciones', '0002_auto_20180814_2152'),
    ]

    operations = [
        migrations.CreateModel(
            name='DetalleSeleccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parcela_seleccionada', models.CharField(max_length=250, verbose_name='Parcela Seleccionado como parcela de aprendizaje?')),
                ('porque', models.TextField()),
                ('encuesta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='herramientaDesiciones.Encuesta')),
            ],
            options={
                'verbose_name_plural': 'Parcela seleccionada de aprendizaje',
            },
        ),
        migrations.RemoveField(
            model_name='detalleplantios',
            name='parcela_seleccionada',
        ),
        migrations.AlterField(
            model_name='detalleplantios',
            name='rendimientos',
            field=models.FloatField(editable=False),
        ),
    ]
