# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from nested_inline.admin import NestedTabularInline, NestedStackedInline, NestedModelAdmin

from .forms import EntrevistadoForm
from .models import *

class InlineComposicionFamilia(NestedStackedInline):
    model = ComposicionFamilia
    extra = 1
    max_num = 1

class InlineServiciosFinca(NestedTabularInline):
    model = ServiciosFinca
    extra = 1
    max_num = 1

class InlineTenencia(NestedTabularInline):
    model = Tenencia
    extra = 1
    max_num = 1

class InlineSeguridadAlimentario(NestedTabularInline):
    model = SeguridadAlimentario
    extra = 1
    max_num = 1

class InlineUsoTierra(NestedTabularInline):
    model = UsoTierra
    extra = 1
    max_num = 8

class InlineRubrosFinca(NestedTabularInline):
    model = RubrosFinca
    extra = 1

class NestedInlineYearEstado(NestedTabularInline):
    model = YearEstado
    fk_name = 'estado'
    extra = 1

class InlineEstadoActual(NestedTabularInline):
    model = EstadoActual
    inlines = [NestedInlineYearEstado]
    extra = 1

class InlineFactores(NestedTabularInline):
    model = Factores
    extra = 1
    max_num = 1

# class InlineVariedad(admin.TabularInline):
#     model = Variedad
#     extra = 1

class InlineLaProduccion(NestedTabularInline):
    model = LaProduccion
    extra = 1
    max_num = 1

class InlineCalidadManejo(NestedTabularInline):
    model = CalidadManejo
    extra = 1
    max_num = 1

class InlineNivelManejo(NestedTabularInline):
    model = NivelManejo
    extra = 1
    max_num = 1

class InlineMesLabores(NestedTabularInline):
    model = MesLabores
    extra = 1
    max_num = 1

class InlineRealizanLabores(NestedTabularInline):
    model = RealizanLabores
    extra = 1
    max_num = 1

class InlineOpcionesAgroecologicas(NestedTabularInline):
    model = OpcionesAgroecologicas
    extra = 1

class InlineCosecha(NestedTabularInline):
    model = Cosecha
    extra = 1
    max_num = 1

class InlineComercializacion(NestedTabularInline):
    model = Comercializacion
    extra = 1
    max_num = 2

class InlineCredito(NestedTabularInline):
    model = Credito
    extra = 1
    max_num = 1

class InlineObtieneCredito(NestedTabularInline):
    model = ObtieneCredito
    extra = 1
    max_num = 1

class InlineMitigacionRiesgo(NestedStackedInline):
    model = MitigacionRiesgo
    extra = 1
    max_num = 1

class InlineCapacitacionTecnicas(NestedTabularInline):
    model = CapacitacionTecnicas
    extra = 1

class InlineCapacitacionSocial(NestedTabularInline):
    model = CapacitacionSocial
    extra = 1

class InlineDetallePlantios(NestedStackedInline):
    model = DetallePlantios
    extra = 1

class InlineDetalleSeleccion(NestedStackedInline):
    model = DetalleSeleccion
    extra = 1

class EncuestaAdmin(NestedModelAdmin):
    form = EntrevistadoForm
    def save_model(self, request, obj, form, change):
      obj.user = request.user
      obj.save()

    exclude = ('user',)
    inlines = [InlineComposicionFamilia,InlineServiciosFinca,InlineTenencia,
                     InlineSeguridadAlimentario,InlineUsoTierra,InlineRubrosFinca,
                     InlineEstadoActual,InlineFactores,InlineLaProduccion,
                     InlineCalidadManejo,InlineNivelManejo,InlineMesLabores,InlineRealizanLabores,
                     InlineOpcionesAgroecologicas,InlineCosecha,InlineComercializacion,
                     InlineCredito,InlineObtieneCredito,InlineMitigacionRiesgo,InlineCapacitacionTecnicas,
                     InlineCapacitacionSocial,InlineDetallePlantios,InlineDetalleSeleccion]

    list_display = ('encuestador','entrevistado','organizacion','get_departamento')
    list_display_links = ('encuestador','entrevistado')
    search_fields = ('entrevistado__nombre',)
    list_filter = ('entrevistado__sexo', 'entrevistado__pais__departamento')

    def get_departamento(self, obj):
        return obj.entrevistado.departamento
    get_departamento.short_description = 'Departamentos'

    class Media:
        css = {
            "all": ("css/my_styles_admin.css",)
        }

class EntrevistadosAdmin(admin.ModelAdmin):
    list_display = ('nombre','sexo','pais','departamento','altitud','maneja')
    search_fields = ('nombre',)
    list_filter = ('sexo','pais','departamento')

# Register your models here.
admin.site.register(Encuestadores)
admin.site.register(Entrevistados, EntrevistadosAdmin)
admin.site.register(Organizacion)
admin.site.register(Encuesta, EncuestaAdmin)
admin.site.register(Rubros)
