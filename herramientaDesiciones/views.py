# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.core.exceptions import ViewDoesNotExist
from django.db.models import Avg, Sum, Count

import json as simplejson
from collections import OrderedDict
import numpy as np

from dal import autocomplete

from herramientaDesiciones.models import *
from herramientaDesiciones.forms import ConsultaHerramientaForm

# Create your views here.
class EntrevistadosAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Entrevistados.objects.none()

        qs = Entrevistados.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

def get_view(request, vista):
    if vista in VALID_VIEWS:
        return VALID_VIEWS[vista](request)
    else:
        raise ViewDoesNotExist("Tried %s in module %s Error: View not defined in VALID_VIEWS." % (vista, 'solidaridad.views'))

def _queryset_filtrado(request):
    params = {}

    if 'fecha' in request.session:
       params['year__in'] = request.session['fecha']

    if 'productor' in request.session:
        params['entrevistado__nombre'] = request.session['productor']

    if 'organizacion' in request.session:
        params['organizacion'] = request.session['organizacion']

    if 'pais' in request.session:
        params['entrevistado__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['entrevistado__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['entrevistado__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['entrevistado__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['entrevistado__sexo'] = request.session['sexo']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return Encuesta.objects.filter(**params)

def HomePageViewHerramienta(request, template='herramientaDesicion/indexherramienta.html'):
    familias = Encuesta.objects.count()
    mujeres = Encuesta.objects.filter(entrevistado__sexo=1).count()
    hombres = Encuesta.objects.filter(entrevistado__sexo=2).count()
    organizacion = Organizacion.objects.count()
    return render(request, template, locals())

def consulta_herramienta(request, template='herramientaDesicion/consultaherramienta.html'):
    if request.method == 'POST':
        form = ConsultaHerramientaForm(request.POST)
        if form.is_valid():
            request.session['fecha'] = form.cleaned_data['fecha']
            #request.session['ciclo'] = form.cleaned_data['ciclo']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['organizacion'] = form.cleaned_data['organizacion']
            request.session['pais'] = form.cleaned_data['pais']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            #request.session['tipologia'] = form.cleaned_data['tipologia']
            centinela = 1
        else:
            centinela = 0
    else:
        form = ConsultaHerramientaForm()
        centinela = 0

        if 'fecha' in request.session:
            try:
                del request.session['fecha']
                #del request.session['ciclo']
                del request.session['productor']
                del request.session['organizacion']
                del request.session['pais']
                del request.session['departamento']
                del request.session['municipio']
                del request.session['comunidad']
                del request.session['sexo']
                #del request.session['tipologia']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela})

def organizacion_pertenece(request, template='herramientaDesicion/educacion.html'):
    filtro = _queryset_filtrado(request)

    tabla_socio = {}
    for obj in CHOICE_SOCIOS:
        contar = filtro.filter(socio__contains=obj[0]).count()
        tabla_socio[obj[1]] = contar

    tabla_organizaciones = []
    for obj in Organizacion.objects.all():
        tabla_organizaciones.append(obj.nombre)

    tabla_desde = {}
    for obj in CHOICE_DESDE_CUANDO:
        contar = filtro.filter(desde_cuando=obj[0]).count()
        tabla_desde[obj[1]] = contar

    return render(request, template, {'tabla_socio':tabla_socio,
                                                                'tabla_organizaciones':tabla_organizaciones,
                                                                'tabla_desde':tabla_desde,
                                                                'num_familias': filtro.count()})

def composicion(request, template='herramientaDesicion/composicion.html'):
    filtro = _queryset_filtrado(request)
    #composcion de la familia
    conteo_adultos_varones = filtro.aggregate(valor=Sum('composicionfamilia__varones_adultos'))['valor']
    conteo_adultos_mujeres = filtro.aggregate(valor=Sum('composicionfamilia__mujeres_adultas'))['valor']
    conteo_varones_jovenes = filtro.aggregate(valor=Sum('composicionfamilia__varones_jovenes'))['valor']
    conteo_mujeres_jovenes = filtro.aggregate(valor=Sum('composicionfamilia__mujeres_jovenes'))['valor']
    conteo_ninos = filtro.aggregate(valor=Sum('composicionfamilia__ninos'))['valor']
    conteo_ninas = filtro.aggregate(valor=Sum('composicionfamilia__ninas'))['valor']
    #relacion finca-vivienda
    conteo_viven_finca = filtro.aggregate(valor=Sum('composicionfamilia__viven_finca'))['valor']
    conteo_viven_pueblo = filtro.aggregate(valor=Sum('composicionfamilia__viven_pueblo'))['valor']
    conteo_viven_ciudad = filtro.aggregate(valor=Sum('composicionfamilia__viven_ciudad'))['valor']
    conteo_viven_finca_casa = filtro.aggregate(valor=Sum('composicionfamilia__viven_finca_casa'))['valor']
    #trabajadres laboran finca
    conteo_permanentes_hombre = filtro.aggregate(valor=Sum('composicionfamilia__permanentes_hombre'))['valor']
    conteo_permanentes_mujeres = filtro.aggregate(valor=Sum('composicionfamilia__permanentes_mujeres'))['valor']
    conteo_temporal_hombre = filtro.aggregate(valor=Sum('composicionfamilia__temporal_hombre'))['valor']
    conteo_temporal_mujeres = filtro.aggregate(valor=Sum('composicionfamilia__temporal_mujeres'))['valor']
    conteo_tecnicos_hombre = filtro.aggregate(valor=Sum('composicionfamilia__tecnicos_hombre'))['valor']
    conteo_tecnicos_mujeres = filtro.aggregate(valor=Sum('composicionfamilia__tecnicos_mujeres'))['valor']

    #grafico de educacion
    grafo_educacion={}
    for obj in CHOICE_EDUCACION:
        conteo = filtro.filter(composicionfamilia__nivel_educacion=obj[0]).count()
        grafo_educacion[obj[1]] = conteo

    return render(request, template, locals())

def servicios_basico(request, template='herramientaDesicion/servicios.html'):
    filtro = _queryset_filtrado(request)
    tabla_electricidad = {}
    for obj in CHOICE_ELECTRICIDAD:
        contar = filtro.filter(serviciosfinca__electricidad__contains=obj[0]).count()
        tabla_electricidad[obj[1]] = contar

    tabla_combustible = {}
    for obj in CHOICE_COMBUSTIBLE:
        contar = filtro.filter(serviciosfinca__combustible__contains=obj[0]).count()
        tabla_combustible[obj[1]] = contar

    tabla_agua_trabajo = {}
    for obj in CHOICE_FUENTE_AGUA:
        contar = filtro.filter(serviciosfinca__agua_trabajo__contains=obj[0]).count()
        tabla_agua_trabajo[obj[1]] = contar

    tabla_agua_consumo = {}
    for obj in CHOICE_FUENTE_AGUA:
        contar = filtro.filter(serviciosfinca__agua_consumo__contains=obj[0]).count()
        tabla_agua_consumo[obj[1]] = contar


    return render(request, template, {'tabla_electricidad':tabla_electricidad,
                                                                'tabla_combustible':tabla_combustible,
                                                                'tabla_agua_trabajo':tabla_agua_trabajo,
                                                                'tabla_agua_consumo':tabla_agua_consumo,
                                                                 'num_familias': filtro.count()})

def tenencia(request, template='herramientaDesicion/tenencia.html'):
    filtro = _queryset_filtrado(request)

    tabla_tenencia= {}
    for obj in CHOICE_TENENCIA:
        contar = filtro.filter(tenencia__tenencia_parcela=obj[0]).count()
        tabla_tenencia[obj[1]] = contar

    tabla_documento_legal = {}
    for obj in CHOICE_DOCUMENTO_LEGAL:
        contar = filtro.filter(tenencia__documento_legal=obj[0]).count()
        tabla_documento_legal[obj[1]] = contar


    return render(request, template, {'tabla_tenencia':tabla_tenencia,
                                                                'tabla_documento_legal':tabla_documento_legal,
                                                                 'num_familias': filtro.count()})

def seguridad_alimentaria(request, template='herramientaDesicion/seguridad_alimentaria.html'):
    filtro = _queryset_filtrado(request)

    tabla_alimento_compra= {}
    for obj in CHOICE_ALIMENTOS_COMPRA:
        contar = filtro.filter(seguridadalimentario__alimentos_basico=obj[0]).count()
        tabla_alimento_compra[obj[1]] = contar

    tabla_cubrir_necesidad = {}
    for obj in CHOICE_SI_NO:
        contar = filtro.filter(seguridadalimentario__cubrir_necesidades_basicas=obj[0]).count()
        tabla_cubrir_necesidad[obj[1]] = contar

    tabla_meses_dificiles = OrderedDict()
    for obj in CHOICE_MESES:
        contar = filtro.filter(seguridadalimentario__meses_dificiles__contains=obj[0]).count()
        porcentaje = (float(contar) / float(filtro.count())) * 100
        tabla_meses_dificiles[obj[1]] = (contar,porcentaje)


    return render(request, template, {'tabla_alimento_compra':tabla_alimento_compra,
                                                                'tabla_cubrir_necesidad':tabla_cubrir_necesidad,
                                                                'tabla_meses_dificiles':tabla_meses_dificiles,
                                                                 'num_familias': filtro.count()})

def tierra(request, template='herramientaDesicion/tierra.html'):
    filtro = _queryset_filtrado(request)

    total_familiar = filtro.count()
    total_manzanas_familias = UsoTierra.objects.filter(encuesta__in=filtro).exclude(tierra=1).aggregate(t=Sum('manzanas'))['t']

    tabla_uso_tierra= OrderedDict()
    for obj in CHOICE_TIERRA[1:]:
        query = filtro.filter(usotierra__tierra = obj[0])
        conteo = query.count()
        porcentaje = saca_porcentajes(conteo, total_familiar)
        manzana = query.aggregate(t=Sum('usotierra__manzanas'))['t']
        porcentaje_mz = saca_porcentajes(manzana,total_manzanas_familias)
        tabla_uso_tierra[obj[1]] = (conteo, porcentaje,manzana,porcentaje_mz)

    return render(request, template, {'tabla_uso_tierra':tabla_uso_tierra,
                                                                'total_manzanas_familias':total_manzanas_familias,
                                                                 'num_familias': filtro.count()})

def rubros(request, template='herramientaDesicion/rubros.html'):
    filtro = _queryset_filtrado(request)

    tabla_rubros= OrderedDict()
    for obj in Rubros.objects.all():
        producen_finca_si = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__producen_finca=1).count()
        producen_finca_no = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__producen_finca=2).count()
        produccion_autoconsumo = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__fin_produccion=1).count()
        produccion_venta = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__fin_produccion=2).count()
        produccion_ambos = filtro.filter(rubrosfinca__rubros = obj,rubrosfinca__fin_produccion=3).count()

        tabla_rubros[obj] = (producen_finca_si,producen_finca_no,produccion_autoconsumo,
                                            produccion_venta,produccion_ambos)

    return render(request, template, {'tabla_rubros':tabla_rubros,
                                                                'num_familias': filtro.count()})

def areas_factores(request, template='herramientaDesicion/areas.html'):
    filtro = _queryset_filtrado(request)

    tabla_areas = OrderedDict()
    for year in CHOICES_ANIOS:
        area_total = YearEstado.objects.filter(estado__encuesta__in=filtro,
                                                                          estado__opciones=1,anio=year[0]).aggregate(t=Sum('valor'))['t']
        desarrollo = YearEstado.objects.filter(estado__encuesta__in=filtro,
                                                                          estado__opciones=2,anio=year[0]).aggregate(t=Sum('valor'))['t']
        produccion = YearEstado.objects.filter(estado__encuesta__in=filtro,
                                                                          estado__opciones=3,anio=year[0]).aggregate(t=Sum('valor'))['t']
        baba = YearEstado.objects.filter(estado__encuesta__in=filtro,
                                                                          estado__opciones=4,anio=year[0]).aggregate(t=Sum('valor'))['t']
        tabla_areas[year[1]] = (area_total,desarrollo,produccion,baba)

    tabla_factor_socioeconomico = OrderedDict()
    for obj in CHOICE_LIMITANTE_PRODUCCION:
        contar = filtro.filter(factores__factor_socioeconomico__contains=obj[0]).count()
        porcentaje = (float(contar) / float(filtro.count())) * 100
        tabla_factor_socioeconomico[obj[1]] = contar

    tabla_factor_biofisico = OrderedDict()
    for obj in CHOICES_LIMITANTE_BIOFISICO:
        contar = filtro.filter(factores__factor_biofisico__contains=obj[0]).count()
        porcentaje = (float(contar) / float(filtro.count())) * 100
        tabla_factor_biofisico[obj[1]] = contar

    return render(request, template, {'tabla_areas':tabla_areas,
                                                                'tabla_factor_socioeconomico':tabla_factor_socioeconomico,
                                                                'tabla_factor_biofisico':tabla_factor_biofisico,
                                                                'num_familias': filtro.count()})

def produccion(request, template='herramientaDesicion/produccion.html'):
    filtro = _queryset_filtrado(request)

    tabla_tiene_vivero = OrderedDict()
    for obj in CHOICE_SI_NO:
        contar = filtro.filter(laproduccion__tiene_vivero=obj[0]).count()
        #porcentaje = (float(contar) / float(filtro.count())) * 100
        tabla_tiene_vivero[obj[1]] = contar

    promedio_plantas = LaProduccion.objects.filter(encuesta__in=filtro).aggregate(p=Avg('cuantas'))['p']

    tabla_variedad = OrderedDict()
    for obj in CHOICES_VARIEDADES:
        contar = filtro.filter(laproduccion__variedad__contains=obj[0]).count()
        #porcentaje = (float(contar) / float(filtro.count())) * 100
        tabla_variedad[obj[1]] = contar

    tabla_semilla = OrderedDict()
    for obj in CHOICES_CONSIGUEN_SEMILLA:
        contar = filtro.filter(laproduccion__consiguen__contains=obj[0]).count()
        #porcentaje = (float(contar) / float(filtro.count())) * 100
        tabla_semilla[obj[1]] = contar

    return render(request, template, {'tabla_tiene_vivero':tabla_tiene_vivero,
                                                                'promedio_plantas':promedio_plantas,
                                                                'tabla_variedad':tabla_variedad,
                                                                'tabla_semilla':tabla_semilla,
                                                                'num_familias': filtro.count()})

def manejo_poda(request, template='herramientaDesicion/manejoPoda.html'):
    filtro = _queryset_filtrado(request)
    #TODO: falta programar este cuadro el 3.4
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__poda=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__poda=obj[0]).count()
        calidad_nivel[obj[1]] = conteo

    calidad_mes = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__poda__contains=obj[0]).count()
        calidad_mes[obj[1]] = conteo

    calidad_realiza = OrderedDict()
    for obj in CHOICES_REALIZAN_LABOR:
        conteo = filtro.filter(realizanlabores__poda__contains=obj[0]).count()
        calidad_realiza[obj[1]] = conteo

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count()})

def manejo_sombra(request, template='herramientaDesicion/manejoSombra.html'):
    filtro = _queryset_filtrado(request)
    #TODO: falta programar este cuadro el 3.4
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__sombra=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__sombra=obj[0]).count()
        calidad_nivel[obj[1]] = conteo

    calidad_mes = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__sombra__contains=obj[0]).count()
        calidad_mes[obj[1]] = conteo

    calidad_realiza = OrderedDict()
    for obj in CHOICES_REALIZAN_LABOR:
        conteo = filtro.filter(realizanlabores__sombra__contains=obj[0]).count()
        calidad_realiza[obj[1]] = conteo

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count()})

def manejo_maleza(request, template='herramientaDesicion/manejoMaleza.html'):
    filtro = _queryset_filtrado(request)
    #TODO: falta programar este cuadro el 3.4
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__maleza=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__maleza=obj[0]).count()
        calidad_nivel[obj[1]] = conteo

    calidad_mes = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__maleza__contains=obj[0]).count()
        calidad_mes[obj[1]] = conteo

    calidad_realiza = OrderedDict()
    for obj in CHOICES_REALIZAN_LABOR:
        conteo = filtro.filter(realizanlabores__maleza__contains=obj[0]).count()
        calidad_realiza[obj[1]] = conteo

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count()})

def manejo_fertilidad(request, template='herramientaDesicion/manejoFertilidad.html'):
    filtro = _queryset_filtrado(request)
    #TODO: falta programar este cuadro el 3.4
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__fertilidad=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__fertilidad=obj[0]).count()
        calidad_nivel[obj[1]] = conteo

    calidad_mes = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__fertilidad__contains=obj[0]).count()
        calidad_mes[obj[1]] = conteo

    calidad_realiza = OrderedDict()
    for obj in CHOICES_REALIZAN_LABOR:
        conteo = filtro.filter(realizanlabores__fertilidad__contains=obj[0]).count()
        calidad_realiza[obj[1]] = conteo

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count()})

def manejo_hongos(request, template='herramientaDesicion/manejoHongos.html'):
    filtro = _queryset_filtrado(request)
    #TODO: falta programar este cuadro el 3.4
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__enfermedad=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__enfermedad=obj[0]).count()
        calidad_nivel[obj[1]] = conteo

    calidad_mes = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__enfermedad__contains=obj[0]).count()
        calidad_mes[obj[1]] = conteo

    calidad_realiza = OrderedDict()
    for obj in CHOICES_REALIZAN_LABOR:
        conteo = filtro.filter(realizanlabores__enfermedad__contains=obj[0]).count()
        calidad_realiza[obj[1]] = conteo

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count()})

def manejo_insectos(request, template='herramientaDesicion/manejoInsectos.html'):
    filtro = _queryset_filtrado(request)
    #TODO: falta programar este cuadro el 3.4
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__plagas_insectos=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__plagas_insectos=obj[0]).count()
        calidad_nivel[obj[1]] = conteo

    calidad_mes = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__plagas_insectos__contains=obj[0]).count()
        calidad_mes[obj[1]] = conteo

    calidad_realiza = OrderedDict()
    for obj in CHOICES_REALIZAN_LABOR:
        conteo = filtro.filter(realizanlabores__plagas_insectos__contains=obj[0]).count()
        calidad_realiza[obj[1]] = conteo

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count()})

def manejo_nematodos(request, template='herramientaDesicion/manejoNematodos.html'):
    filtro = _queryset_filtrado(request)
    #TODO: falta programar este cuadro el 3.4
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__plagas_nematodos=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__plagas_nematodos=obj[0]).count()
        calidad_nivel[obj[1]] = conteo

    calidad_mes = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__plagas_nematodos__contains=obj[0]).count()
        calidad_mes[obj[1]] = conteo

    calidad_realiza = OrderedDict()
    for obj in CHOICES_REALIZAN_LABOR:
        conteo = filtro.filter(realizanlabores__plagas_nematodos__contains=obj[0]).count()
        calidad_realiza[obj[1]] = conteo

    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel':calidad_nivel,
                                                                'calidad_mes':calidad_mes,
                                                                'calidad_realiza':calidad_realiza,
                                                                'num_familias': filtro.count()})


def opcion_agroecologica(request, template='herramientaDesicion/opcion_agro.html'):
    filtro = _queryset_filtrado(request)

    tabla_opcion_agro = OrderedDict()
    for obj in CHOICES_OPCIONES_AGRO:
        query = filtro.filter(opcionesagroecologicas__opciones=obj[0])
        no_utiliza = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=1).count()
        pequena_escala = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=2).count()
        mayor_escala = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=3).count()
        toda_finca = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=4).count()
        tabla_opcion_agro[obj[1]] = (no_utiliza,pequena_escala,mayor_escala,toda_finca)

    return render(request, template, {'tabla_opcion_agro':tabla_opcion_agro,
                                                                'num_familias': filtro.count()})

def cosecha(request, template='herramientaDesicion/cosecha.html'):
    filtro = _queryset_filtrado(request)

    tabla_cortes = OrderedDict()
    for obj in CHOICES_CORTES:
        valor = filtro.filter(cosecha__cortes=obj[0]).count()
        tabla_cortes[obj[1]] = valor

    tabla_mazorca = OrderedDict()
    for obj in CHOICES_SEPARAN_MAZORCA:
        valor = filtro.filter(cosecha__separan_mazorca=obj[0]).count()
        tabla_mazorca[obj[1]] = valor

    tabla_quiebran = OrderedDict()
    for obj in CHOICES_QUIEBRAN_MAZORCA:
        valor = filtro.filter(cosecha__quiebran_mazorca=obj[0]).count()
        tabla_quiebran[obj[1]] = valor

    tabla_calidad = OrderedDict()
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(cosecha__calidad=obj[0]).count()
        tabla_calidad[obj[1]] = valor

    tabla_precio = OrderedDict()
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(cosecha__precio=obj[0]).count()
        tabla_precio[obj[1]] = valor

    promedio_precio = Cosecha.objects.filter(encuesta__in=filtro).aggregate(p=Avg('cuanto'))['p']


    return render(request, template, {'tabla_cortes':tabla_cortes,
                                                                'tabla_mazorca':tabla_mazorca,
                                                                'tabla_quiebran':tabla_quiebran,
                                                                'tabla_calidad':tabla_calidad,
                                                                'tabla_precio':tabla_precio,
                                                                'promedio_precio':promedio_precio,
                                                                'num_familias': filtro.count()})

def comercializacion(request, template='herramientaDesicion/comercializacion.html'):
    filtro = _queryset_filtrado(request)

    tabla_comercializacion = {}
    for obj in CHOICES_COMERCIALIZACION:
        cantidad_total = filtro.filter(comercializacion__opcion=obj[0]).aggregate(t=Sum('comercializacion__cantidad_total'))['t']
        intermediario = filtro.filter(comercializacion__opcion=obj[0]).aggregate(t=Sum('comercializacion__intermediario'))['t']
        inter_precio = filtro.filter(comercializacion__opcion=obj[0]).aggregate(t=Avg('comercializacion__intermediario_precio'))['t']
        coop_cantidad = filtro.filter(comercializacion__opcion=obj[0]).aggregate(t=Sum('comercializacion__coop_cantidad'))['t']
        coop_precio = filtro.filter(comercializacion__opcion=obj[0]).aggregate(t=Sum('comercializacion__coop_precio'))['t']
        tabla_comercializacion[obj[1]] = (cantidad_total,intermediario,inter_precio,coop_cantidad,coop_precio)


    return render(request, template, {'tabla_comercializacion':tabla_comercializacion,
                                                                'num_familias': filtro.count()})

def credito(request, template='herramientaDesicion/credito.html'):
    filtro = _queryset_filtrado(request)

    tabla_credito = {}
    for obj in CHOICES_CREDITO:
        monto_credito_corto = filtro.filter(credito__opcion=obj[0]).aggregate(t=Avg('credito__monto_credito_corto'))['t']
        monto_credito_mediano = filtro.filter(credito__opcion=obj[0]).aggregate(t=Avg('credito__monto_credito_mediano'))['t']
        monto_credito_largo = filtro.filter(credito__opcion=obj[0]).aggregate(t=Avg('credito__monto_credito_largo'))['t']

        tabla_credito[obj[1]] = (monto_credito_corto,monto_credito_mediano,monto_credito_largo)

    tabla_facilidad = {}
    for obj in CHOICES_FACILIDAD:
        valor = filtro.filter(credito__facilidad=obj[0]).count()
        tabla_facilidad[obj[1]] = valor

    tabla_obtiene_credito = {}
    for obj in CHOICES_OBTIENE_CREDITO:
        valor = filtro.filter(obtienecredito__obtiene_credito__contains=obj[0]).count()
        tabla_obtiene_credito[obj[1]] = valor


    return render(request, template, {'tabla_credito':tabla_credito,
                                                                'tabla_facilidad':tabla_facilidad,
                                                                'tabla_obtiene_credito':tabla_obtiene_credito,
                                                                'num_familias': filtro.count()})

def mitigacion(request, template='herramientaDesicion/mitigacion.html'):
    filtro = _queryset_filtrado(request)

    tabla_monitorio_plaga = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__monitoreo_plaga=obj[0]).count()
        tabla_monitorio_plaga[obj[1]] = valor

    tabla_monitorio_cada_cuanto = {}
    for obj in CHOICES_CADA_CUANTO:
        valor = filtro.filter(mitigacionriesgo__cada_cuanto=obj[0]).count()
        tabla_monitorio_cada_cuanto[obj[1]] = valor

    tabla_monitorio_realiza = {}
    for obj in CHOICES_COMO_REALIZA:
        valor = filtro.filter(mitigacionriesgo__como_realiza=obj[0]).count()
        tabla_monitorio_realiza[obj[1]] = valor

    tabla_lleva_registro = {}
    for obj in CHOICES_LLEVA_REGISTROS:
        valor = filtro.filter(mitigacionriesgo__lleva_registro=obj[0]).count()
        tabla_lleva_registro[obj[1]] = valor

    tabla_suficiente_recurso = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__cuenta_recursos=obj[0]).count()
        tabla_suficiente_recurso[obj[1]] = valor

    tabla_falta_recurso = {}
    for obj in CHOICES_FALTA_RECURSO:
        valor = filtro.filter(mitigacionriesgo__falta_recurso__contains=obj[0]).count()
        tabla_falta_recurso[obj[1]] = valor

    tabla_obra_almacenamiento = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__obras_almacenamiento=obj[0]).count()
        tabla_obra_almacenamiento[obj[1]] = valor

    tabla_venta_organizada = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__venta_organizada=obj[0]).count()
        tabla_venta_organizada[obj[1]] = valor

    tabla_contrato_venta = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__contrato_venta=obj[0]).count()
        tabla_contrato_venta[obj[1]] = valor

    tabla_certificado = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__certificado=obj[0]).count()
        tabla_certificado[obj[1]] = valor

    tabla_tipo_certificado = {}
    for obj in CHOICES_TIPO_CERTIFICADO:
        valor = filtro.filter(mitigacionriesgo__tipo_certificado__contains=obj[0]).count()
        tabla_tipo_certificado[obj[1]] = valor

    tabla_calidad_cacao = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__calidad_cacao=obj[0]).count()
        tabla_calidad_cacao[obj[1]] = valor

    tabla_plan_manejo = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__plan_manejo=obj[0]).count()
        tabla_plan_manejo[obj[1]] = valor

    tabla_plan_negocio = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__plan_negocio=obj[0]).count()
        tabla_plan_negocio[obj[1]] = valor

    tabla_plan_inversion = {}
    for obj in CHOICE_SI_NO:
        valor = filtro.filter(mitigacionriesgo__plan_inversion=obj[0]).count()
        tabla_plan_inversion[obj[1]] = valor

    tabla_apoya_planes = {}
    for obj in CHOICES_APOYA_PLANES:
        valor = filtro.filter(mitigacionriesgo__apoya_planes__contains=obj[0]).count()
        tabla_apoya_planes[obj[1]] = valor


    return render(request, template, {'tabla_monitorio_plaga':tabla_monitorio_plaga,
                                                                'tabla_monitorio_cada_cuanto':tabla_monitorio_cada_cuanto,
                                                                'tabla_monitorio_realiza':tabla_monitorio_realiza,
                                                                'tabla_lleva_registro':tabla_lleva_registro,
                                                                'tabla_suficiente_recurso':tabla_suficiente_recurso,
                                                                'tabla_falta_recurso':tabla_falta_recurso,
                                                                'tabla_obra_almacenamiento':tabla_obra_almacenamiento,
                                                                'tabla_venta_organizada':tabla_venta_organizada,
                                                                'tabla_contrato_venta':tabla_contrato_venta,
                                                                'tabla_certificado':tabla_certificado,
                                                                'tabla_tipo_certificado':tabla_tipo_certificado,
                                                                'tabla_calidad_cacao':tabla_calidad_cacao,
                                                                'tabla_plan_manejo':tabla_plan_manejo,
                                                                'tabla_plan_negocio':tabla_plan_negocio,
                                                                'tabla_plan_inversion':tabla_plan_inversion,
                                                                'tabla_apoya_planes':tabla_apoya_planes,
                                                                'num_familias': filtro.count()})

def capasitacion_tecnica(request, template='herramientaDesicion/capasitaciones.html'):
    filtro = _queryset_filtrado(request)

    tabla_tecnica = OrderedDict()
    for obj in CHOICES_CAPACITACION_TECNICA:
        total_veces =  filtro.filter(capacitaciontecnicas__opcion=obj[0]).aggregate(t=Sum('capacitaciontecnicas__cuantas_veces'))['t']
        pobre = filtro.filter(capacitaciontecnicas__opcion=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=1).count()
        algo = filtro.filter(capacitaciontecnicas__opcion=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=2).count()
        regular = filtro.filter(capacitaciontecnicas__opcion=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=3).count()
        bueno = filtro.filter(capacitaciontecnicas__opcion=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=4).count()
        excelente = filtro.filter(capacitaciontecnicas__opcion=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=5).count()

        tabla_tecnica[obj[1]] = (total_veces,pobre,algo,regular,bueno,excelente)

    return render(request, template, {'tabla_tecnica':tabla_tecnica,
                                                                'num_familias': filtro.count()})

def capasitacion_social(request, template='herramientaDesicion/capasitacion_social.html'):
    filtro = _queryset_filtrado(request)

    tabla_social = OrderedDict()
    for obj in CHOICES_TEMAS_SOCIAL:
        total_veces =  filtro.filter(capacitacionsocial__tema=obj[0]).aggregate(t=Sum('capacitacionsocial__cuantas_veces'))['t']
        pobre = filtro.filter(capacitacionsocial__tema=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=1).count()
        algo = filtro.filter(capacitacionsocial__tema=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=2).count()
        regular = filtro.filter(capacitacionsocial__tema=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=3).count()
        bueno = filtro.filter(capacitacionsocial__tema=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=4).count()
        excelente = filtro.filter(capacitacionsocial__tema=obj[0],
                                         capacitaciontecnicas__nivel_consideracion=5).count()

        tabla_social[obj[1]] = (total_veces,pobre,algo,regular,bueno,excelente)

    return render(request, template, {'tabla_social':tabla_social,
                                                                'num_familias': filtro.count()})


def detalles_conteo_parcela(request, template='herramientaDesicion/detallesConteoParcela.html'):
    filtro = _queryset_filtrado(request)

    #numero de plantios
    total_plantios = filtro.aggregate(t=Count('detalleplantios__nombre_plantio'))['t']

    conteos_plantios = []
    for obj in filtro:
        conteo = obj.detalleplantios_set.all().count()
        conteos_plantios.append(conteo)
    # media arítmetica
    promedio = np.mean(conteos_plantios)
    # mediana
    mediana = np.median(conteos_plantios)
    #minimo y maximo
    minimo = min(conteos_plantios)
    maximo = max(conteos_plantios)

    lista_reducida = list(set(conteos_plantios))
    grafo_parcelas = OrderedDict()
    for obj in lista_reducida:
        conteo = contar_veces(obj, conteos_plantios)
        grafo_parcelas[obj] = conteo

    return render(request, template, {'grafo_parcelas':grafo_parcelas,
                                                                'numero_plantio':total_plantios,
                                                                'promedio': promedio,
                                                                'mediana': mediana,
                                                                'minimo':minimo,
                                                                'maximo':maximo,
                                                                'num_familias': filtro.count()})


def detalles_area_parcela(request, template='herramientaDesicion/detallesAreaParcela.html'):
    filtro = _queryset_filtrado(request)

    total_areas_plantios = filtro.aggregate(t=Sum('detalleplantios__area'))['t']

    conteos_areas_plantios = []
    for obj in filtro:
        for i in obj.detalleplantios_set.all():
            conteos_areas_plantios.append(i.area)

    # media arítmetica
    promedio = np.mean(conteos_areas_plantios)
    # mediana
    mediana = np.median(conteos_areas_plantios)
    #minimo y maximo
    minimo = min(conteos_areas_plantios)
    maximo = max(conteos_areas_plantios)

    grafo_area_plantios = crear_rangos(request, conteos_areas_plantios, minimo, maximo, step=2)

    return render(request, template, {'numero_plantio':total_areas_plantios,
                                                                'promedio': promedio,
                                                                'mediana': mediana,
                                                                'minimo':minimo,
                                                                'maximo':maximo,
                                                                'grafo_area_plantios':grafo_area_plantios,
                                                                'num_familias': filtro.count()})

def detalles_area_cacao(request, template='herramientaDesicion/detallesAreaCacao.html'):
    filtro = _queryset_filtrado(request)

    total_areas_plantios = filtro.aggregate(t=Sum('detalleplantios__area'))['t']

    conteos_areas_plantios = []
    for obj in filtro:
        suma = 0
        for i in obj.detalleplantios_set.all():
            suma += i.area
        conteos_areas_plantios.append(suma)
    # media arítmetica
    promedio = np.mean(conteos_areas_plantios)
    # mediana
    mediana = np.median(conteos_areas_plantios)
    #minimo y maximo
    minimo = min(conteos_areas_plantios)
    maximo = max(conteos_areas_plantios)

    grafo_area_plantios = crear_rangos(request, conteos_areas_plantios, minimo, maximo, step=10)

    #   lo mismo pero para desarrollo de platas en cacao
    total_areas_plantios_D = filtro.filter(detalleplantios__tipo_planta=1).aggregate(t=Sum('detalleplantios__area'))['t']
    conteos_areas_plantios_D = []
    for obj in filtro:
        suma_d = 0
        for i in obj.detalleplantios_set.all():
            if i.tipo_planta == 1:
                suma_d += i.area
        conteos_areas_plantios_D.append(suma_d)
    # media arítmetica
    promedio_D = np.mean(conteos_areas_plantios_D)
    # mediana
    mediana_D = np.median(conteos_areas_plantios_D)
    #minimo y maximo
    minimo_D = min(conteos_areas_plantios_D)
    maximo_D = max(conteos_areas_plantios_D)

    grafo_area_plantios_D = crear_rangos(request, conteos_areas_plantios_D, minimo_D, maximo_D, step=10)

    # para productiva
    total_areas_plantios_P = filtro.filter(detalleplantios__tipo_planta=2).aggregate(t=Sum('detalleplantios__area'))['t']

    conteos_areas_plantios_P = []
    for obj in filtro:
        suma_p = 0
        for i in obj.detalleplantios_set.all():
            if i.tipo_planta == 2:
                suma_p += i.area
        conteos_areas_plantios_P.append(suma_p)
    # media arítmetica
    promedio_P = np.mean(conteos_areas_plantios_P)
    # mediana
    mediana_P = np.median(conteos_areas_plantios_P)
    #minimo y maximo
    minimo_P = min(conteos_areas_plantios_P)
    maximo_P = max(conteos_areas_plantios_P)
    grafo_area_plantios_P = crear_rangos(request, conteos_areas_plantios_P, minimo_P, maximo_P, step=10)

    #para mezcla
    total_areas_plantios_M = filtro.filter(detalleplantios__tipo_planta=3).aggregate(t=Sum('detalleplantios__area'))['t']

    conteos_areas_plantios_M = []
    for obj in filtro:
        suma_m = 0
        for i in obj.detalleplantios_set.all():
            if i.tipo_planta == 3:
                suma_m += i.area
        conteos_areas_plantios_M.append(suma_m)
    # media arítmetica
    promedio_M = np.mean(conteos_areas_plantios_M)
    # mediana
    mediana_M = np.median(conteos_areas_plantios_M)
    #minimo y maximo
    minimo_M = min(conteos_areas_plantios_M)
    maximo_M = max(conteos_areas_plantios_M)
    grafo_area_plantios_M = crear_rangos(request, conteos_areas_plantios_M, minimo_M, maximo_M, step=10)

    return render(request, template, {'numero_plantio':total_areas_plantios,
                                                                'promedio': promedio,
                                                                'mediana': mediana,
                                                                'minimo':minimo,
                                                                'maximo':maximo,
                                                                'grafo_area_plantios':grafo_area_plantios,
                                                                #desarrollo
                                                                'numero_plantio_d':total_areas_plantios_D,
                                                                'promedio_d': promedio_D,
                                                                'mediana_d': mediana_D,
                                                                'minimo_d':minimo_D,
                                                                'maximo_d':maximo_D,
                                                                'grafo_area_plantios_d':grafo_area_plantios_D,
                                                                #productivo
                                                                'numero_plantio_p':total_areas_plantios_P,
                                                                'promedio_p': promedio_P,
                                                                'mediana_p': mediana_P,
                                                                'minimo_p':minimo_P,
                                                                'maximo_p':maximo_P,
                                                                'grafo_area_plantios_p':grafo_area_plantios_P,
                                                                #mezcla
                                                                 'numero_plantio_m':total_areas_plantios_M,
                                                                'promedio_m': promedio_M,
                                                                'mediana_m': mediana_M,
                                                                'minimo_m':minimo_M,
                                                                'maximo_m':maximo_M,
                                                                'grafo_area_plantios_m':grafo_area_plantios_M,
                                                                'num_familias': filtro.count()})

def detalles_produccion_cacao(request, template='herramientaDesicion/detallesProduccionCacao.html'):
    filtro = _queryset_filtrado(request)

    total_produccion_plantios = filtro.aggregate(t=Sum('detalleplantios__produccion'))['t']
    conteos_produccion_plantios = []
    for obj in filtro:
        suma = 0
        for i in obj.detalleplantios_set.all():
            suma += i.produccion
        conteos_produccion_plantios.append(suma)
    # media arítmetica
    promedio = np.mean(conteos_produccion_plantios)
    # mediana
    mediana = np.median(conteos_produccion_plantios)
    #minimo y maximo
    minimo = min(conteos_produccion_plantios)
    maximo = max(conteos_produccion_plantios)
    grafo_produccion_plantios = crear_rangos(request, conteos_produccion_plantios, minimo, maximo, step=10)

    #produccion
    total_produccion_plantios_P = filtro.filter(detalleplantios__tipo_planta=2).aggregate(t=Sum('detalleplantios__produccion'))['t']
    conteos_produccion_plantios_P = []
    for obj in filtro:
        suma = 0
        for i in obj.detalleplantios_set.all():
            if i.tipo_planta == 2:
                suma += i.produccion
        conteos_produccion_plantios_P.append(suma)
    # media arítmetica
    promedio_P = np.mean(conteos_produccion_plantios_P)
    # mediana
    mediana_P = np.median(conteos_produccion_plantios_P)
    #minimo y maximo
    minimo_P = min(conteos_produccion_plantios_P)
    maximo_P = max(conteos_produccion_plantios_P)
    grafo_produccion_plantios_P = crear_rangos(request, conteos_produccion_plantios_P, minimo_P, maximo_P, step=10)

    #mezcla
    total_produccion_plantios_M = filtro.filter(detalleplantios__tipo_planta=3).aggregate(t=Sum('detalleplantios__produccion'))['t']
    conteos_produccion_plantios_M = []
    for obj in filtro:
        suma = 0
        for i in obj.detalleplantios_set.all():
            if i.tipo_planta == 3:
                suma += i.produccion
        conteos_produccion_plantios_M.append(suma)
    # media arítmetica
    promedio_M = np.mean(conteos_produccion_plantios_M)
    # mediana
    mediana_M = np.median(conteos_produccion_plantios_M)
    #minimo y maximo
    minimo_M = min(conteos_produccion_plantios_M)
    maximo_M = max(conteos_produccion_plantios_M)
    grafo_produccion_plantios_M = crear_rangos(request, conteos_produccion_plantios_M, minimo_M, maximo_M, step=10)

    return render(request, template, {'numero_plantio':total_produccion_plantios,
                                                                'promedio': promedio,
                                                                'mediana': mediana,
                                                                'minimo':minimo,
                                                                'maximo':maximo,
                                                                'grafo_produccion_plantios':grafo_produccion_plantios,
                                                                #productivo
                                                                'numero_plantio_p':total_produccion_plantios_P,
                                                                'promedio_p': promedio_P,
                                                                'mediana_p': mediana_P,
                                                                'minimo_p':minimo_P,
                                                                'maximo_p':maximo_P,
                                                                'grafo_produccion_plantios_p':grafo_produccion_plantios_P,
                                                                #mezcla
                                                                 'numero_plantio_m':total_produccion_plantios_M,
                                                                'promedio_m': promedio_M,
                                                                'mediana_m': mediana_M,
                                                                'minimo_m':minimo_M,
                                                                'maximo_m':maximo_M,
                                                                'grafo_produccion_plantios_m':grafo_produccion_plantios_M,
                                                                'num_familias': filtro.count()})

def detalles_edad_cacao(request, template='herramientaDesicion/detallesEdadCacao.html'):
    filtro = _queryset_filtrado(request)

    conteos_edad_plantios = []
    for obj in filtro:
        for i in obj.detalleplantios_set.all():
            if i.tipo_planta == 2:
                conteos_edad_plantios.append(i.edad)

    #minimo y maximo
    try:
        minimo = min(conteos_edad_plantios)
    except:
        minimo = 0
    try:
        maximo = max(conteos_edad_plantios)
    except:
        maximo = 0
    grafo_edad_plantios = crear_rangos(request, conteos_edad_plantios, minimo, maximo, step=5)

    conteos_edad_plantios_M = []
    for obj in filtro:
        for i in obj.detalleplantios_set.all():
            if i.tipo_planta == 3:
                conteos_edad_plantios_M.append(i.edad)

    #minimo y maximo
    try:
        minimo_M = min(conteos_edad_plantios_M)
    except:
        minimo_M = 0
    try:
        maximo_M = max(conteos_edad_plantios_M)
    except:
        maximo_M = 0
    grafo_edad_plantios_M = crear_rangos(request, conteos_edad_plantios_M, minimo_M, maximo_M, step=5)

    return render(request, template, {'grafo_edad_plantios':grafo_edad_plantios,
                                                                'grafo_edad_plantios_m': grafo_edad_plantios_M,
                                                                'num_familias': filtro.count()})

def detalles_variedad_cacao(request, template='herramientaDesicion/detallesVariedadCacao.html'):
    filtro = _queryset_filtrado(request)

    tabla_variedad = {}
    for obj in CHOICES_VARIEDAD_PREDOMINANTE:
        valor = filtro.filter(detalleplantios__variedades__contains=obj[0]).count()
        tabla_variedad[obj[1]] = valor

    tabla_sombra = {}
    for obj in CHOICE_TIPO_SOMBRA:
        valor = filtro.filter(detalleplantios__tipo_sombra=obj[0]).count()
        tabla_sombra[obj[1]] = valor

    tabla_fertilizacion = {}
    for obj in CHOICE_TIPO_FERTILIZACION:
        valor = filtro.filter(detalleplantios__tipo_fertilizacion=obj[0]).count()
        tabla_fertilizacion[obj[1]] = valor

    tabla_fungicida = {}
    for obj in CHOICE_TIPO_FUNGICIDA:
        valor = filtro.filter(detalleplantios__tipo_fungicida=obj[0]).count()
        tabla_fungicida[obj[1]] = valor

    lista_monilia = []
    for obj in filtro:
        for i in obj.detalleplantios_set.all():
            lista_monilia.append(i.nivel)
    grafo_monilia = crear_rangos(request, lista_monilia, min(lista_monilia), max(lista_monilia), step=10)

    lista_nivel_sombra = []
    for obj in filtro:
        for i in obj.detalleplantios_set.all():
            lista_nivel_sombra.append(i.nivel_sombra)
    grafo_sombra = crear_rangos(request, lista_nivel_sombra, min(lista_nivel_sombra), max(lista_nivel_sombra), step=10)

    return render(request, template, {'tabla_variedad':tabla_variedad,
                                                                'tabla_sombra':tabla_sombra,
                                                                'tabla_fertilizacion':tabla_fertilizacion,
                                                                'tabla_fungicida':tabla_fungicida,
                                                                'grafo_monilia': grafo_monilia,
                                                                'grafo_sombra':grafo_sombra,
                                                                'num_familias': filtro.count()})

def detalles_dispercion_cacao(request, template='herramientaDesicion/detallesDispercionCacao.html'):
    filtro = _queryset_filtrado(request)

    dispersion_prod_area = []
    dispersion_prod_monilia = []
    dispersion_prod_sombra = []
    for obj in filtro:
        for i in obj.detalleplantios_set.all():
            dispersion_prod_area.append([i.area,i.produccion])
            dispersion_prod_monilia.append([i.nivel,i.rendimientos])
            dispersion_prod_sombra.append([i.nivel_sombra,i.rendimientos])

    dispercion_fertilidad = OrderedDict()
    for obj in CHOICE_TIPO_FERTILIZACION:
        rendimiento = DetallePlantios.objects.filter(encuesta__in=filtro, tipo_fertilizacion=obj[0]).aggregate(t=Avg('rendimientos'))['t']
        if rendimiento > 0:
            dispercion_fertilidad[obj[1]] = rendimiento

    dispercion_fungicida = OrderedDict()
    for obj in CHOICE_TIPO_FUNGICIDA:
        rendimiento = DetallePlantios.objects.filter(encuesta__in=filtro, tipo_fungicida=obj[0]).aggregate(t=Avg('rendimientos'))['t']
        if rendimiento > 0:
            dispercion_fungicida[obj[1]] = rendimiento

    return render(request, template, {'dispersion_prod_area':dispersion_prod_area,
                                                                'dispersion_prod_monilia': dispersion_prod_monilia,
                                                                'dispersion_prod_sombra':dispersion_prod_sombra,
                                                                'dispercion_fertilidad':dispercion_fertilidad,
                                                                'dispercion_fungicida':dispercion_fungicida,
                                                                'num_familias': filtro.count()})

def detalles_tabla(request, template='herramientaDesicion/detallesTabla.html'):
    filtro = _queryset_filtrado(request)

    tabla_detalles = []
    for obj in filtro:
        for i in obj.detalleplantios_set.all():
            tabla_detalles.append([i.nombre_plantio,
                                                     i.area,
                                                     i.get_tipo_planta_display(),
                                                     i.edad,
                                                     i.variedades,
                                                     i.produccion,
                                                     i.nivel,
                                                     i.get_tipo_sombra_display(),
                                                     i.nivel_sombra,
                                                     i.get_tipo_fertilizacion_display(),
                                                     i.get_tipo_fungicida_display(),
                                                     i.rendimientos,
                                                                         ])

    return render(request, template, {'tabla_detalles':tabla_detalles,
                                                                'num_familias': filtro.count()})

def contar_veces(elemento, lista):
    veces = 0
    for i in lista:
        if elemento == i:
            veces += 1
    return veces

def crear_rangos(request, lista, start=0, stop=0, step=0):
    dict_algo = OrderedDict()
    rangos = []
    contador = 0
    rangos = [(n, n+int(step)-1) for n in range(int(start), int(stop), int(step))]

    for desde, hasta in rangos:
        dict_algo['%s a %s' % (desde,hasta)] = len([x for x in lista if desde <= x <= hasta])

    return dict_algo

VALID_VIEWS = {
        'organizacion': organizacion_pertenece,
        'composicion': composicion,
        'servicios': servicios_basico,
        'tenencia': tenencia,
        'seguridad': seguridad_alimentaria,
        'tierra': tierra,
        'rubros': rubros,
        'areas': areas_factores,
        'produccion': produccion,
        'manejo_poda':manejo_poda,
        'manejo_sombra':manejo_sombra,
        'manejo_maleza':manejo_maleza,
        'manejo_fertilidad':manejo_fertilidad,
        'manejo_hongos':manejo_hongos,
        'manejo_insectos':manejo_insectos,
        'manejo_nematodos':manejo_nematodos,
        'agroecologicas':opcion_agroecologica,
        'cosecha': cosecha,
        'comercializacion':comercializacion,
        'credito': credito,
        'mitigacion':mitigacion,
        'tecnicas':capasitacion_tecnica,
        'sociales':capasitacion_social,
        'detalles_conteo_parcela': detalles_conteo_parcela,
        'detalles_area_parcela': detalles_area_parcela,
        'detalles_area_cacao': detalles_area_cacao,
        'detalles_produccion_cacao':detalles_produccion_cacao,
        'detalles_edad_cacao': detalles_edad_cacao,
        'detalles_variedad_cacao':detalles_variedad_cacao,
        'detalles_dispercion_cacao':detalles_dispercion_cacao,
        'detalles_tabla':detalles_tabla,
    }


def obtener_lista_mapa_cacao(request):
    if request.is_ajax():
        lista = []
        for objeto in Encuesta.objects.all():
            dicc = dict(nombre=objeto.entrevistado.nombre, id=objeto.id,
                        lon=float(objeto.entrevistado.longitud) ,
                        lat=float(objeto.entrevistado.latitud)
                        )
            lista.append(dicc)

        serializado = simplejson.dumps(lista)
        return HttpResponse(serializado, content_type='application/json')

def saca_porcentajes(dato, total, formato=True):
    '''Si formato es true devuelve float caso contrario es cadena'''
    if dato != None:
        try:
            porcentaje = (dato/float(total)) * 100 if total != None or total != 0 else 0
        except:
            return 0
        if formato:
            return porcentaje
        else:
            return '%.2f' % porcentaje
    else:
        return 0

def get_productor(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        personas = Entrevistados.objects.filter(nombre__icontains = q )[:10]
        #print personas
        results = []
        for person in personas:
            personas_json = {}
            personas_json['id'] = person.id
            personas_json['label'] = person.nombre
            personas_json['value'] = person.nombre
            results.append(personas_json)
    else:
        results = 'fail'
    return HttpResponse(simplejson.dumps(results), content_type='application/json')
